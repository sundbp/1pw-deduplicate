#!/usr/bin/env bb

(require '[cheshire.core :as json])

(defn get-item
  [uuid]
  (let [r (shell/sh "op" "get" "item" uuid "--fields" "website,username,password,tags")]
    (assert (zero? (:exit r)) (format "Could not get item %s" uuid))
    (-> r
        :out
        (json/parse-string true)
        (assoc :uuid uuid))))

(defn get-items
  [vault]
  (let [r (shell/sh "op" "list" "items" "--categories" "Login" "--vault" vault)
        _ (assert (zero? (:exit r)) "Couldn't get items via 'op'.")
        r (-> r :out (json/parse-string true))
        ;; r (-> (slurp "/tmp/short.json") (json/parse-string true))
        ]
    (->> r
         (remove #(= "Y" (:trashed %)))
         (map (fn [{:keys [uuid] :as x}] (get-item uuid))))))

(defn remove-http
  [items]
  (map (fn [x]
         (let [ws (:website x)
               m (re-matches #"http[s]*:\/\/(.*)" ws)
               r (if m (second m) ws)]
           (assoc x :website r)))
       items))

(defn delete-item
  [uuid]
  (let [r (shell/sh "op" "delete" "item" uuid)]
    (assert (zero? (:exit r)) (format "Could not delete item %s" uuid))))

(defn remove-duplicates
  [infos]
  (doseq [{:keys [website discard keep]} infos]
    (println (format "For %s removing %d items.." website (count discard)))
    (doseq [{:keys [uuid]} discard]
      (delete-item uuid))))

(let [items (get-items "Private")
      items (remove-http items)
      by-website (group-by :website items)
      removal-info (->> by-website
                        (remove (fn [[_ hits]] (= 1 (count hits))))
                        (remove (fn [[_ hits]]
                                  (let [normalized (map (fn [x] (dissoc x :uuid :tags)) hits)]
                                    (not (apply = normalized)))))
                        (map (fn [[website hits]]
                               (if (= 2 (count hits))
                                 (let [discard (->> hits
                                                    (filter (fn [{:keys [tags]}]
                                                              (some #(= "1PIF Import 18-05-2021" %) tags)))
                                                    first)
                                       keep (if (= discard (first hits))
                                              (second hits)
                                              (first hits))]
                                   {:website website :discard [discard] :keep keep})
                                 (do
                                   (println (format "REMOVING MORE THAN 1 ENTRY FOR %s" website))
                                   {:website website :discard (into [] (drop 1 hits)) :keep (first hits)})))))]
  (println (format "Number of Websites to remove something for: %d" (count removal-info)))
  (println "Removal info:")
  (clojure.pprint/print-table [:website :keep :discard] removal-info)
  (println "\nGo ahead and remove? ")
  (let [answer (read-line)]
    (when (= "yes" answer)
      (remove-duplicates removal-info))))

(System/exit 0)
