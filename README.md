# 1pw-deduplicate

This tool removes duplicate entries from 1password.

Duplicate is determined by comparing items after removing their "id" fields, 
using Clojure's `=` function.

# Usage

Steps to use this tool:

1. Setup 1password-cli and ensure the `op` command is operational.
2. Ensure you have a working version of [babashka](https://github.com/babashka/babashka) installed.
3. Make sure you have a backup of your 1pw data!
4. Run the tool `./1pw-deduplicate.clj`
5. Inspect the changes and approve applying these changes to your 1pw setup (or abort).

If there's a problem you find after having applied changes to your vault any deleted entries can be found in the Trash. 
